var Server = require('socket.io');
var PORT   = 3030;

var fs = require('fs')

var staticFileServer = require( "./lib/static-file-server" );
var chalk = require( "chalk" );

// Create an instance of our static file server.
var fileServer = staticFileServer.createServer({

    // I tell the static file server which directory to use when resolving paths.
    documentRoot: ( __dirname + "/" ),

    // I tell the static file server which default document to use when the user requests
    // a directory instead of a file.
    defaultDocument: "index.htm",

    // I tell the static file server the max-age of the Cache-Control header.
    maxAge: 604800, // 7 days.

    // I tell the static file server which portions of the URL path to strip out before
    // resolving the path against the document root. This allows parts of the URL to serve
    // as a cache-busting mechanism without having to alter the underlying file structure.
    magicPattern: /build-[\d.-]+/i,

    // I tell the static file server the maximum size of the file that can be cached in
    // memory (larger files will be piped directly from the file system).
    maxCacheSize: ( 1024 * 100 ) // 100Kb.

});


var server = require('http').Server(function(req,res){

//    fs.readFile('./demo/demo.html', function (err, data) {
//        if (err) throw err;
//        res.writeHead(200, {'Content-Type': 'text/html' });
//        res.write(data)
//        res.end()
//    });

    fileServer.serveFile( req, res );


});

var io = Server(PORT);

io.close(); // Close current server

server.listen(PORT); // PORT is free to use 

io = Server(server);

// ユーザ管理ハッシュ
var userHash = {};


io.sockets.on("connection", function (socket) {

    // 接続開始カスタムイベント(接続元ユーザを保存し、他ユーザへ通知)
    socket.on("connected", function (name) {
        var msg = name + "が入室しました";
        userHash[socket.id] = name;
        console.log(userHash)
        //io.sockets.emit("publish", {value: msg});
    });

    // 陈述
    socket.on("publishMessageToPro", function (data) {
        console.log("publishMessageToPro data is coming " + data.value)
        io.sockets.emit("statementFromCon", {value: data.value});
    });


    // 陈述
    socket.on("publishMessageToCon", function (data) {
        console.log("publishMessageToCon data is coming " + data.value)
        io.sockets.emit("statementFromPro", {value: data.value});
    });

   //分析
   socket.on('publishAnalysis',function(data){
       console.log("analysis data is comint "+ data.value)
       io.sockets.emit("analysis",{value: data.value})
   })

    //分析
    socket.on('publishAnalysisToPro',function(data){
        console.log("analysis data is comint "+ data.value)
        io.sockets.emit("analysisFromCon",{value: data.value})
    })


    // 接続終了組み込みイベント(接続元ユーザを削除し、他ユーザへ通知)
    socket.on("disconnect", function () {
        if (userHash[socket.id]) {
            var msg = userHash[socket.id] + "が退出しました";
            delete userHash[socket.id];

            io.sockets.emit("publish", {value: msg});
        }
    });

})